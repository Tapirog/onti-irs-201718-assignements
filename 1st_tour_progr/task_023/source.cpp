#include <iostream>
#include <vector>
#include <set>


using namespace std;


set <int> s;

bool contains(int n) {
    return s.find(n) != s.end();
}

int main() {
	int n;
	cin >> n;
	int now = 0;
	vector <int> v(n);
	for (auto &i : v) {
	    cin >> i;
	    s.insert(i);
	    now ^= i;
	}
	for (int i = 0; ; i++) {
	    if (i != (i ^ now) && !contains(i) && !contains(i ^ now)) {
	        cout << i << ' ' << (i ^ now) << '\n';
	        return 0;
	    }
	}
	return 0;
}	