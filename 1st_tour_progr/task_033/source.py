from math import sqrt

def calc(m, x0, x, r):
    return m * m <= r * r - (x - x0) * (x - x0)


def get_y(x0, y0, r, x):
    _l = -1
    _r = r + 2
    while _r - _l > 1:
        m = (_l + _r) // 2
        if m * m <= r * r - (x - x0) * (x - x0):
            _l = m
        else:
            _r = m
    return _l

x1, y1, r1 = map(int, input().split())
x2, y2, r2 = map(int, input().split())
ans = 0
for x in range(x1-r1, x1+r1+1):
    if x > x2 + r2 or x < x2 - r2:
        continue

    y1l = int(y1 - sqrt(r1 ** 2 - (x1 - x) ** 2)) - 10
    while ((y1l - y1) ** 2 + (x1 - x) ** 2) > r1 ** 2:
        y1l += 1

    y1h = int(y1 + sqrt(r1 ** 2 - (x1 - x) ** 2)) + 10
    while ((y1h - y1) ** 2 + (x1 - x) ** 2) > r1 ** 2:
        y1h -= 1

    y2l = int(y2 - sqrt(r2 ** 2 - (x2 - x) ** 2)) - 10
    while ((y2l - y2) ** 2 + (x2 - x) ** 2) > r2 ** 2:
        y2l += 1

    y2h = int(y2 + sqrt(r2 ** 2 - (x2 - x) ** 2)) + 10
    while ((y2h - y2) ** 2 + (x2 - x) ** 2) > r2 ** 2:
        y2h -= 1

    if not (y1h < y2l or y2h < y1l):
        # print((y1l, y1h), (y2l, y2h))
        # print(x, abs(max(y1l, y2l) - min(y1h, y2h)) + 1)
        ans += abs(max(y1l, y2l) - min(y1h, y2h)) + 1

print(ans)
