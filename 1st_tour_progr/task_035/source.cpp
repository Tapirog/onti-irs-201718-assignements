#include <bits/stdc++.h>

using namespace std;
typedef  long long ll;
typedef unsigned long long ull;
int inf_int=1e8;
ll inf_ll=1e18;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
const double pi=3.1415926535898;

const int MAXN=3e5+10;

int parent[MAXN];
int rank[MAXN];
int find_parent(int v) {
    if(v==parent[v]) {
        return v;
    }
    return parent[v]=find_parent(parent[v]);
}

bool union_set(int a,int b) {
    a=find_parent(a);
    b=find_parent(b);
    if(a!=b) {
        if(rank[a]<rank[b])
            swap(a,b);
        else if(rank[a]==rank[b])
            ++rank[a];
        parent[b]=a;
        return true;
    }
    return false;
}
int x[MAXN],y[MAXN];
char used[MAXN];
int a[MAXN];
int ans[MAXN];
void solve()
{
    int n,m,q;
    scanf("%d",&n);
    scanf("%d",&m);
    for(int i=1;i<=m;++i) {
        scanf("%d %d",&x[i],&y[i]);
    }
    scanf("%d",&q);
    for(int i=1;i<=q;++i) {
        scanf("%d",&a[i]);
        used[a[i]]=1;
    }
    for(int i=1;i<=n;++i)
        parent[i]=i;

    int cur=n;
    for(int i=1;i<=m;++i) {
        if(!used[i]) {
            cur-=union_set(x[i],y[i]);
        }
    }
    for(int i=q;i>=1;--i) {
        ans[i]=cur;
        cur-=union_set(x[a[i]],y[a[i]]);
    }
    for(int i=1;i<=q;++i) {
        printf("%d ",ans[i]);
    }


}

int main()
{
        int t=1;
        while(t--)
           solve();
        return 0;
}