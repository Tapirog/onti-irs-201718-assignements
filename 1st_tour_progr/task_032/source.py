A = input().split();
n = int(A[0]);
k = int(A[1]);

s = input();

cnt = [0] * 26;

for i in range(n) :
    cnt[ord(s[i]) - 97] = cnt[ord(s[i]) - 97] + 1;

mx = 0;

for i in range(26) :
    if (cnt[i] > cnt[mx]) :
        mx = i;

for i in range(k) :
    s = s + chr(97 + mx);

print(s);
