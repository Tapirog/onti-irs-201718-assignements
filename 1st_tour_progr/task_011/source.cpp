#include <iostream>
#include <cmath>

using namespace std;

int main() {
    int av, ah, bv, bh;
    cin >> av >> ah >> bv >> bh;
    cout << max(abs(av - bv), abs(ah - bh)) << '\n';
}
