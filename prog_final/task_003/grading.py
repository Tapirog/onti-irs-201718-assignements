def generate():
    return []

def check(reply, clue):
    a = float(reply)
    b = float(clue)
    return abs(a - b) / max(1.0, b) <= 1e-6

from collections import namedtuple
from math import sqrt

Point = namedtuple("Point", ["x", "y"])

# triangle area
def area(a, b, c):
    s = (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)
    return -1 if s < 0 else (1 if s > 0 else 0)

# bounding box
def box(a, b, c, d):
    return max(min(a, b), min(c, d)) <= min(max(a, b), max(c, d))

# пересечение отрезков
def intersect(a, b, c, d):
    return box(a.x, b.x, c.x, d.x) & box(a.y, b.y, c.y, d.y) \
           & (area(a, b, c) * area(a, b, d) <= 0) & (area(c, d, a) * area(c, d, b) <= 0)

# расстояние между точками a и b
def dist(a, b):
    return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y))

# расстояние между точкой с и отрезком ab
def dist2(a, b, c):
    A = a.y - b.y
    B = b.x - a.x
    C = b.y * a.x - a.y * b.x
    cc = B * c.x - A * c.y
    d = A * A + B * B
    p = Point((cc * B - C * A) / d, -(cc * A + C * B) / d)

    return dist(c, p) if dist(a, p) + dist(p, b) - dist(a, b) < 1e-7 else min(dist(c, a), dist(c, b))

# расстояние между отрезками ab и cd
def dist3(a, b, c, d):
    return min(min(dist2(c, d, a), dist2(c, d, b)), min(dist2(a, b, c), dist2(a, b, d)))

def solve(dataset):
    ds = dataset.splitlines()

    s = ds[0].split()
    n = int(s[0])
    m = int(s[1])

    p = [[]]
    for i in range(n):
        s = ds[i + 1].split()
        p.append([])
        for j in range(m):
            p[i].append(Point(int(s[j * 2]), int(s[j * 2 + 1])))

    inter = False

    for i in range(n):
        for j in range(i + 1, n):
            for ii in range(m - 1):
                for jj in range(m - 1):
                    inter |= intersect(p[i][ii], p[i][ii + 1], p[j][jj], p[j][jj + 1])

    if inter:
        return str(-1)

    ans = 1e15

    for i in range(n):
        for j in range(i + 1, n):
            for ii in range(m - 1):
                for jj in range(m - 1):
                    ans = min(ans, dist3(p[i][ii], p[i][ii + 1], p[j][jj], p[j][jj + 1]))

    return str(ans / 2.0)