
def turnRight(initialPosition):
    return (initialPosition + 1) % 4

def turnLeft(initialPosition):
    return (initialPosition - 1) % 4

def symmetricAdd(room, a, b, c):
    if c == 3:
        room[1][b - 1] = a
    elif c == 2:
        room[0][b - 1] = a
    elif c == 1:
        room[3][b - 1] = a
    else:
        room[2][b - 1] = a

n, k, l = map(int, input().strip().split())
rooms = []
for i in range(4):
    rooms.append([-1 for j in range(n)])
for i in range(k):
    a, b, c = map(int, input().strip().split())
    rooms[c][a-1] = b
    symmetricAdd(rooms, a, b, c)
initialPosition, initialDirection = map(int, input().strip().split())
newRooms = []
for i in range(4):
    newRooms.append([-1 for j in range(n)])

currentRoom = initialPosition
currentDirection = initialDirection
turnFlag = True

for i in range(l):
    left, middle, right = map(int, input().strip().split())
    if left == middle == right == 3:
        currentDirection = turnRight(currentDirection)
        turnFlag = True
    elif left == middle == right == 2:
        currentDirection = turnLeft(currentDirection)
        turnFlag = True
    else:
        if not turnFlag:
            currentRoom = newRooms[currentDirection][currentRoom - 1]
        if left == 1:
            newRooms[turnLeft(currentDirection)][currentRoom-1] = \
                rooms[turnLeft(currentDirection)][currentRoom-1]
            symmetricAdd(newRooms, currentRoom,
                    rooms[turnLeft(currentDirection)][currentRoom-1],
                         turnLeft(currentDirection))
        if right == 1:
            newRooms[turnRight(currentDirection)][currentRoom - 1] = \
                rooms[turnRight(currentDirection)][currentRoom - 1]
            symmetricAdd(newRooms, currentRoom,
                         rooms[turnRight(currentDirection)][currentRoom - 1],
                         turnRight(currentDirection))
        if middle == 1:
            newRooms[currentDirection][currentRoom - 1] = \
                rooms[currentDirection][currentRoom - 1]
            symmetricAdd(newRooms, currentRoom,
                    rooms[currentDirection][currentRoom - 1],
                    currentDirection)
        turnFlag = False

allRooms = set()
allNewRooms = set()
for i in range(4):
    for j in newRooms[i]:
        if j != -1:
            allNewRooms.add(j)
for i in range(4):
    for j in rooms[i]:
        if j != -1:
            allRooms.add(j)

hiddenRooms = allRooms.difference(allNewRooms)
sortedHiddenRooms = sorted(list(hiddenRooms))

for i in sortedHiddenRooms:
    print(i, end = " ")