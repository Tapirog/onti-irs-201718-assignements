#include <iostream>
#include <cmath>
#include <stdio.h>

using namespace std;

float PI = 3.14159265;
float PIdiv2 = PI / 2;
float a, blueColor, c;
float xi, yi;

char hexValueColor[8];
int redColor, greenColor, blueColor;
int grayColor = 100;
int N, M;

bool binaryImg[1000][1000];
int borders[1000][1000];
int bordersPoints[100000][2];
int positionCount = 0;
int xMin, yMin, xMax, yMax;
int xMin2, yMin2, xMax2, yMax2;
int xP, yP, xC, yC;
int cornersCoodinates[4][2];
int NumberOfCordes[9][2];
bool matrix[3][3];
int decodeValue = 0;

void RGBRead()
{
    char rhex[] = { hexValueColor[1],hexValueColor[2], };
    char ghex[] = { hexValueColor[3],hexValueColor[4], };
    char bhex[] = { hexValueColor[5],hexValueColor[6], };  
	
    sscanf(rhex, "%x", &redColor);
    sscanf(ghex, "%x", &greenColor);
    sscanf(bhex, "%x", &blueColor);
}

void calculateCoefficients(float x1, float y1, float x2, float y2)
{
    a = y2 - y1;
    blueColor = x1 - x2;
    c = y1*x2 - y2*x1;
}

int solveEquation(float a2, float b2, float c2)
{
    if (a*b2 - a2*blueColor == 0)
        return 1;
    xi = (c*b2 - c2*blueColor) / (a*b2 - a2*blueColor)*-1;
    yi = (a*c2 - a2*c) / (a*b2 - a2*blueColor)*-1;
    return 0;
}

void findBoarder()
{
    for (int ia = 0; ia < N; ia++)
    {
        for (int ib = 0; ib < M; ib++)
        {
            if (binaryImg[ia][ib] == 1)
            {
                if (ia > yMax)
                {
                    yMax = ia;
                    yMax2 = ib;
                }
                if (ia < yMin)
                {
                    yMin = ia;
                    yMin2 = ib;
                }
                if (ib > xMax)
                {
                    xMax = ib;
                    xMax2 = ia;
                }
                if (ib < xMin)
                {
                    xMin = ib;
                    xMin2 = ia;
                }
                if (borders[ia][ib] != 1)
                {
                    borders[ia][ib] = 1;

                    bordersPoints[positionCount][0] = ib;
                    bordersPoints[positionCount][1] = ia;
                    positionCount++;
                }

                break;
            }
        }
    }

    for (int ia = 0; ia < N; ia++)
    {
        for (int ib = M - 1; ib >= 0; ib--)
        {
            if (binaryImg[ia][ib] == 1)
            {
                if (ia > yMax)
                {
                    yMax = ia;
                    yMax2 = ib;
                }
                if (ia < yMin)
                {
                    yMin = ia;
                    yMin2 = ib;
                }
                if (ib > xMax)
                {
                    xMax = ib;
                    xMax2 = ia;
                }
                if (ib < xMin)
                {
                    xMin = ib;
                    xMin2 = ia;
                }
                if (borders[ia][ib] != 1)
                {
                    borders[ia][ib] = 1;

                    bordersPoints[positionCount][0] = ib;
                    bordersPoints[positionCount][1] = ia;
                    positionCount++;
                }

                break;
            }
        }
    }

    for (int ib = 0; ib < N; ib++)
    {
        for (int ia = M - 1; ia >= 0; ia--)
        {
            if (binaryImg[ia][ib] == 1)
            {
                if (ia > yMax)
                {
                    yMax = ia;
                    yMax2 = ib;
                }
                if (ia < yMin)
                {
                    yMin = ia;
                    yMin2 = ib;
                }
                if (ib > xMax)
                {
                    xMax = ib;
                    xMax2 = ia;
                }
                if (ib < xMin)
                {
                    xMin = ib;
                    xMin2 = ia;
                }
                if (borders[ia][ib] != 1)
                {
                    borders[ia][ib] = 1;
    
                    bordersPoints[positionCount][0] = ib;
                    bordersPoints[positionCount][1] = ia;
                    positionCount++;
                }
                break;
            }
        }
    }

    for (int ib = 0; ib < N; ib++)
    {
        for (int ia = 0; ia < M; ia++)
        {
            if (binaryImg[ia][ib] == 1)
            {
                if (ia > yMax)
                {
                    yMax = ia;
                    yMax2 = ib;
                }
                if (ia < yMin)
                {
                    yMin = ia;
                    yMin2 = ib;
                }
                if (ib > xMax)
                {
                    xMax = ib;
                    xMax2 = ia;
                }
                if (ib < xMin)
                {
                    xMin = ib;
                    xMin2 = ia;
                }
                if (borders[ia][ib] != 1)
                {
                    borders[ia][ib] = 1;

                    bordersPoints[positionCount][0] = ib;
                    bordersPoints[positionCount][1] = ia;
                    positionCount++;
                }

                break;
            }
        }
    }
}

int calculetePoints()
{
    if (binaryImg[yMin][xMin] == 1)
    {
        xP = xMin;
        yP = yMin;
        return 0;
    }
    if (binaryImg[yMin][xMax] == 1)
    {
        xP = xMax;
        yP = yMin;
        return 0;
    }
    if (binaryImg[yMax][xMin] == 1)
    {
        xP = xMin;
        yP = yMax;
        return 0;
    }
    if (binaryImg[yMax][xMax] == 1)
    {
        xP = xMax;
        yP = yMax;
        return 0;
    }
    if (binaryImg[yMax][yMax2] == 1)
    {
        xP = yMax2;
        yP = yMax;
        return 0;
    }
    if (binaryImg[yMin][yMin2] == 1)
    {
        xP = yMin2;
        yP = yMin;
        return 0;
    }
    if (binaryImg[xMax2][xMax] == 1)
    {
        xP = xMax;
        yP = xMax2;
        return 0;
    }
    if (binaryImg[xMin2][xMin] == 1)
    {
        xP = xMin;
        yP = xMin2;
        return 0;
    }
    return 1;
}

int findCorners()
{
    cornersCoodinates[0][0] = xP;
    cornersCoodinates[0][1] = yP;
    
    float dist = sqrt(pow(xC - xP, 2) + pow(yC - yP, 2));
    
    double cosx;
    double sinx;
    float a;
    
    if (dist != 0)
    {
        cosx = (yC - xP) / dist;
        sinx = (xC - yP) / dist;
    }
    else
        return -2;
    
    if (sinx >= 0)
        a = acos(cosx);
    else
        a = 2 * PI - acos(cosx);
    
    for (int i = 0; i < 4; i++)
        {
        float maxgyp = 0;
        int mdotx = 0, mdoty = 0;
        float x1, y1, x2, y2;
        x1 = xC;
        y1 = yC;
        x2 = xC + 10 * cos(a);
        y2 = yC + 10 * sin(a);
        
        calculateCoefficients(x1, y1, x2, y2);
        
        for (int ia = 0; ia < positionCount; ia++)
        {
            int xd, yd;
            xd = bordersPoints[ia][0];
            yd = bordersPoints[ia][1];
            
            float dist = sqrt(pow(xC - xd, 2) + pow(yC - yd, 2));
            
            double cosx;
            double sinx;
            float ad;
            
            if (dist != 0)
            {
                cosx = (yd - yC) / dist;
                sinx = (xd - xC) / dist;
            }
            else
                return -2;
            
            if (sinx >= 0)
                ad = acos(cosx);
            else
                ad = 2 * PI - acos(cosx);
            
            float dx = ad - a;
            
            if (sin(dx) < 0)
            {
                solveEquation(0, 1, (yd*-1));
                float gyp = sqrt(pow(xi - xd, 2) + pow(yi - yd, 2));
                    
                if (gyp > maxgyp)
                {
                    maxgyp = gyp;
                    mdotx = xd;
                    mdoty = yd;
                }
            }
        }

        cornersCoodinates[i][0] = mdotx;
        cornersCoodinates[i][1] = mdoty;
        
        a = a + PIdiv2;
    }
}

void GetNumberOfCoords()
{
    int realcenterx, realcentery;
    float a2, b2, c2;
    
    calculateCoefficients(cornersCoodinates[0][0], 
            cornersCoodinates[0][1], cornersCoodinates[2][0], 
            cornersCoodinates[2][1]);
    a2 = a; b2 = blueColor; c2 = c;
    calculateCoefficients(cornersCoodinates[1][0], 
            cornersCoodinates[1][1], cornersCoodinates[3][0], 
            cornersCoodinates[3][1]);
    
    solveEquation(a2, b2, c2);
    realcenterx = xi; realcentery = yi;
    
    int CucQuad[4][2];
    for (int i = 0; i < 4; i++)
    {
        CucQuad[i][0] = (cornersCoodinates[i][0] - realcenterx) / 2.4 + realcenterx;
        CucQuad[i][1] = (cornersCoodinates[i][1] - realcentery) / 2.4 + realcentery;
    }
    
    NumberOfCordes[2][0] = CucQuad[0][0];
    NumberOfCordes[2][1] = CucQuad[0][1];
    NumberOfCordes[1][0] = (CucQuad[0][0] + CucQuad[1][0]) / 2;
    NumberOfCordes[1][1] = (CucQuad[0][1] + CucQuad[1][1]) / 2;
    
    NumberOfCordes[0][0] = CucQuad[1][0];
    NumberOfCordes[0][1] = CucQuad[1][1];
    NumberOfCordes[3][0] = (CucQuad[1][0] + CucQuad[2][0]) / 2;
    NumberOfCordes[3][1] = (CucQuad[1][1] + CucQuad[2][1]) / 2;
    
    NumberOfCordes[6][0] = CucQuad[2][0];
    NumberOfCordes[6][1] = CucQuad[2][1];
    NumberOfCordes[7][0] = (CucQuad[2][0] + CucQuad[3][0]) / 2;
    NumberOfCordes[7][1] = (CucQuad[2][1] + CucQuad[3][1]) / 2;
    
    NumberOfCordes[8][0] = CucQuad[3][0];
    NumberOfCordes[8][1] = CucQuad[3][1];
    NumberOfCordes[5][0] = (CucQuad[3][0] + CucQuad[0][0]) / 2;
    NumberOfCordes[5][1] = (CucQuad[3][1] + CucQuad[0][1]) / 2;
    
    NumberOfCordes[4][0] = realcenterx;
    NumberOfCordes[4][1] = realcentery;
}


void decode()
{
    int icuc = 0;
    
    for (int iy = 0; iy<3; iy++)
    {
        for (int ix = 0; ix<3; ix++)
        {
            matrix[ix][iy] = binaryImg[NumberOfCordes[icuc][1]][NumberOfCordes[icuc][0]];
            icuc++;
        }
    }

    if (matrix[2][2] == 0)
    {
    bool code[4] = { matrix[1][2],matrix[2][1],matrix[0][1],matrix[1][0], };
        for (int i = 0; i<4; i++)
        {
            decodeValue += pow(2, i)*code[i];
        }
    }
    if (matrix[0][2] == 0)
    {
    bool code[4] = { matrix[0][1],matrix[1][2],matrix[1][0],matrix[2][1], };
        for (int i = 0; i<4; i++)
        {
            decodeValue += pow(2, i)*code[i];
        }
    }
    if (matrix[0][0] == 0)
    {
    bool code[4] = { matrix[1][0],matrix[0][1],matrix[2][1],matrix[1][2], };
        for (int i = 0; i<4; i++)
        {
            decodeValue += pow(2, i)*code[i];
        }
    }
    if (matrix[2][0] == 0)
    {
    bool code[4] = { matrix[2][1],matrix[1][0],matrix[1][2],matrix[0][1], };
        for (int i = 0; i<4; i++)
        {
            decodeValue += pow(2, i)*code[i];
        }
    }
}


int main()
{
    cin >> M >> N;
    
    for (int ia = 0; ia < N; ia++)
    {
        for (int ib = 0; ib < M; ib++)
        {
        cin >> hexValueColor; RGBRead();
            if (redColor < grayColor && greenColor < grayColor && blueColor < grayColor)
            {
            binaryImg[ia][ib] = 1;
            xMin = ib; xMax = ib; xMin2 = ia; xMax2 = ia;
            yMin = ia; yMax = ia; yMin2 = ib; yMax2 = ib;
            }
        else
            binaryImg[ia][ib] = 0;
        }
    }
    
    findBoarder();
    if (calculetePoints() == 1) cout << "[ ! ] getPoints() failed\n";
    
    xC = (xMax + xMin) / 2;
    yC = (yMax + yMin) / 2;
    
    findCorners();
    GetNumberOfCoords();
    
    decode();
    
    cout << decodeValue << endl;
}
